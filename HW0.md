1) What excites me about this course is the fact that I will be more comfortable operating in this space, and will be able to take advantage of these tools that are available not only here at Stanford but will help accelerate my career in general.
2) I have few worries about this course, other than the fact that I will make mistakes but this is natural in any course.
3) I am really curious about learning about making repositories and transferring data between clusters and cloud storage, as this is a very practical and crucial  skill to learn in virtually any context.
4) As neural datasets become larger and freely available, data management will be a prerequisite skill of any neuroscience researcher.
5) I hope to work on either some datasets that I have been exposed to during my rotation, or even other high quality neural datasets that have been made available (via the IBL, NWB, HCP, etc).
